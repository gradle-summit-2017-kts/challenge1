plugins {
    java
    `maven-publish`
}

publishing {

    // publications is a PublicationContainer which is a (Polymorphic)NamedDomainObjectContainer<Publication>
    (publications) { // WAIT, WAT?

        "mavenJava"(MavenPublication::class) {
            from(components["java"])
        }

    }
}


